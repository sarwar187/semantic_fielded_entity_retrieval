EYRE18 code for the submission titled as Semantic Fielded Entity Retrieval 
==============================

Steps to run the proposed system:

1. At first run the query transformer to get bag-of-terms and bag-of-entity queries from the provided queries-v2_stopped.txt file.
   use the following commmand from the project root: python2.7 code/data/query_transformer.

2. then use python2.7 normalize_score data/runs/fsdm_v2.run command to normalize the score of fsdm_v2 run provided by DBpedia V2 dataset.
   As we are re-ranking the results from them we normalize their score and combine it with our semantic_component_score_v2.run and
   term_semantic_component_score_v2.run.


Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── corpus         <- corpus.txt
    │   ├── embedding      <- joint embedding file is placed here
    │   ├── queries        <- queries and their bag of term and bag of entity based representations are here
    │   └── runs           <- runs are here
    │
    ├── models             <- our model is based on average of word embedding and coordinate ascent.
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── code               <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- our model is based on average of word embedding and coordinate ascent.
    │   │   ├── CoordinateAscent         <- corpus.txt
    │   │              predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

