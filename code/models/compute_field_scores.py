import numpy as np
import pandas as pd
import sys
reload(sys)
sys.setdefaultencoding('utf8')

"""
This script is for loading the score of each field of a document against a query and writing them to a run file.
"""

home_directory = "../../data/embedding/field_embeddings/"
#home_directory = "/home/smsarwar/work/eyre18/"
directory_list=["attributes", "names", "related_entities", "category", "text", "redirect"]
attributes_score = np.loadtxt(open(home_directory + "attributes/score.csv", "rb"), delimiter=",")
print "attribute loaded"
names_score = np.loadtxt(open(home_directory + "names/score.csv", "rb"), delimiter=",")
print "names loaded"
related_entities_score = np.loadtxt(open(home_directory + "related_entities/score.csv", "rb"), delimiter=",")
print "related entities loaded"
category_score = np.loadtxt(open(home_directory + "category/score.csv", "rb"), delimiter=",")
print "category loaded"
text_score = np.loadtxt(open(home_directory + "text/score.csv", "rb"), delimiter=",")
print "text loaded"
redirect_score = np.loadtxt(open(home_directory + "redirect/score.csv", "rb"), delimiter=",")
print "redirect loaded"

#doc = np.loadtxt(open("/home/smsarwar/work/eyre18/document_keys.csv", "rb"), delimiter=",")
#query = np.loadtxt(open("/home/smsarwar/work/eyre18/query_keys.csv", "rb"), delimiter=",")

document_keys = []
query_keys = []

for line in open(home_directory + "document_keys.csv", "rb"):
    line = line.strip()
    document_keys.append(line)

print len(document_keys)

for line in open(home_directory + "query_keys.csv", "rb"):
    line = line.strip()
    query_keys.append(line)

#print len(query_keys)
#print query_keys
count_missing = 0
count = 0

run_directory = "../../data/"
detailed_score_file = open(home_directory + "semantic_component_score_v2.run", "w")
for line in open(home_directory + "fsdm_v2.run", "rb"):
    line_splitted = line.split(" ")
    query = line_splitted[0].strip()
    doc = line_splitted[2].strip()
    count+=1
    if count%1000==0:
        print str(count) + " line_processed"
        print count_missing
    score = 0.0
    score_string = "0.0 0.0 0.0 0.0 0.0\t0.0"
    if doc in document_keys:
        q = query_keys.index(query)
        d = document_keys.index(doc)
        score = attributes_score[q][d] + names_score[q][d] + related_entities_score[q][d] + category_score[q][d] + \
                text_score[q][d] + redirect_score[q][d]
        score_string = str(attributes_score[q][d]) + "\t" + str(names_score[q][d]) + "\t" + str(related_entities_score[q][d]) + "\t" + str(category_score[q][d]) + \
                "\t" + str(text_score[q][d]) + "\t" + str(redirect_score[q][d])

    str_to_write = line_splitted[0] + " " + line_splitted[1] + " " + line_splitted[2] + " " + line_splitted[3] + " " + str(score_string) + " " + "semantic"
    detailed_score_file.write(str_to_write+"\n")

