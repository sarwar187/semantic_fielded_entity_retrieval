import gensim
import numpy as np
import re
from gensim.models import KeyedVectors
import socket

class compute_embedding(object):
    def __init__(self, fileName=None):

        #self.embedding = KeyedVectors.load_word2vec_format(
        #    '/home/smsarwar/work/fastText/build/model_mincount5.bin', limit=100000, binary=False)
        #self.embedding = FastText.load_fasttext_format("/home/smsarwar/work/fastText/build/model_mincount5.bin")
        #if socket.gethostname()=='brooloo':
        if fileName==None:
            #self.embedding = KeyedVectors.load_word2vec_format('/home/smsarwar/PycharmProjects/GoogleNews-vectors-negative300.bin.gz', limit=100000, binary=True)
            self.embedding = gensim.models.Word2Vec.load_word2vec_format('./model/GoogleNews-vectors-negative300.bin', binary=True)
            #self.embedding = KeyedVectors.load_word2vec_format(fileName, limit=100000, binary=True)
        else:
            #self.embedding = KeyedVectors.load_word2vec_format(fileName, limit=100000, binary=True)
            self.embedding = KeyedVectors.load(fileName, mmap='r')
        print("model loaded")

    def get_average_embedding(self, tokens, embedding_size):
        """
        return the word embedding of a set of tokens. used for average sentence embedding
        :param tokens:
        :return: average of word embedding for a set of tokens
        """
        # regex = re.compile(r"""
        # (?<!\S)   # Assert there is no non-whitespace before the current character
        # (?:       # Start of non-capturing group:
        #  [^\W\d_] # Match either a letter
        #  [\w-]*   # followed by any number of the allowed characters
        # |         # or
        #  \d+      # match a string of digits.
        # )         # End of group
        # (?!\S)    # Assert there is no non-whitespace after the current character""",
        #                    re.VERBOSE)

        mismatch_count = 0
        match_count = 0
        embeddings = []
        #print tokens
        #print (len(tokens))
        for token in tokens:
            token_lower = token.lower()
            #print token_lower
            if token_lower in self.embedding:
                embeddings.append(self.embedding[token_lower.strip()])
                match_count+=1
            else:
                mismatch_count+=1
        #embeddings = np.asarray(
        #    [self.embedding[token.strip()] for token in tokens if token in self.embedding and regex.match(token)])
        #print(embeddings)
        # if we dont find embedding for any token in a sentence set all to zero
        embeddings = np.asarray(embeddings)
        if len(embeddings) == 0:
            embeddings = np.ones((1, embedding_size))
        return np.average(embeddings, axis=0), mismatch_count, match_count

