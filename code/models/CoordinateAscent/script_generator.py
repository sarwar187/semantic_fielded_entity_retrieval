import numpy as np
import subprocess as sp

"""
This script generates all the train, validation and test scripts for learning model parameters.
"""

query_types = ["INEX_LD", "ListSearch", "QALD2", "SemSearch-ES",  "all_queries"]

#file = open("script_full.sh", "w")
result = {}
for query_type in query_types:
    result.setdefault(query_type, {})
    ndcg_cut_10 = 0
    ndcg_cut_20 = 0
    ndcg_cut_10_detail = 0
    ndcg_cut_20_detail = 0
    count = 0
    for fold in np.arange(5):
        str_to_write = "/home/smsarwar/anaconda2/bin/python ca_learner.py -o /home/smsarwar/results_ca.run "
        training_file_location = "../../../data/train_test_splits/" + query_type + "/fold" + str(fold) + "/train_file"
        validation_file_location = "../../../data/train_test_splits/" + query_type + "/fold" + str(fold) + "/validation_file"
        test_file_location = "../../../data/train_test_splits/" + query_type + "/fold" + str(fold) + "/test_file"
        test_non_svmlight_file_location = "../../../data/train_test_splits/" + query_type + "/fold" + str(fold) + "/test_non_svmlight_file"
        test_file_directory = "../../../data/train_test_splits/" + query_type + "/fold" + str(fold) + "/"
        str_to_write+=training_file_location + " " + validation_file_location + " " + test_file_location + " " + test_non_svmlight_file_location + " " + test_file_directory
        print str_to_write
        output = sp.check_output(str_to_write.split())
        output_split = output.split("\n")
        #print output_split
        fold_result = output_split[0].split("\t")
        #print fold_result
        result[query_type][fold] = (float(fold_result[0]), float(fold_result[1]))
        ndcg_cut_10+=result[query_type][fold][0]
        ndcg_cut_20 += result[query_type][fold][1]

        ndcg_cut_10_detail_string = output_split[1].split("\t")
        ndcg_cut_20_detail_string = output_split[2].split("\t")
        #print ndcg_cut_10_detail_string
        #print ndcg_cut_20_detail_string
        for ndcg_val in ndcg_cut_10_detail_string:
            ndcg_cut_10_detail+=float(ndcg_val)

        for ndcg_val in ndcg_cut_20_detail_string:
            ndcg_cut_20_detail += float(ndcg_val)
            count+=1

        #print result[query_type][fold]

    average_ndcg_cut_10 = ndcg_cut_10/5
    average_ndcg_cut_20 = ndcg_cut_20/5
    average_ndcg_cut_10_detail = ndcg_cut_10_detail/count
    average_ndcg_cut_20_detail = ndcg_cut_20_detail/count

    print(str(average_ndcg_cut_10) + "\t" + str(average_ndcg_cut_20) + "\t" + str(average_ndcg_cut_10_detail) + "\t" + str(average_ndcg_cut_20_detail))

        #print(output)