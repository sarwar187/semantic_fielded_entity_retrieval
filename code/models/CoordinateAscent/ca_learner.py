from __future__ import print_function, division
import argparse
from sklearn.datasets import load_svmlight_file
from coordinate_ascent import CoordinateAscent
from metrics import DCGScorer, NDCGScorer
import numpy as np

from utils import load_docno, print_trec_run

def load_run_into_dictionary(run_file, number_of_fields):
    """
    This method loads a run file and returns a dictionary [query][document] = (score_tuple)
    :param run_file:
    """
    run_dict = {}
    with open(run_file) as f:
        for line in f:
            splitted = line.strip().split(' ')
            query = splitted[0]
            entity = splitted[2]
            field_scores = []
            for i in np.arange(number_of_fields):
                field_scores.append(float(splitted[i+4]))
            run_dict.setdefault(query, {})
            run_dict[query][entity] = field_scores
    return run_dict

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-nv', '--no-validation', action='store_true',
                        help='do not use validation data')
    parser.add_argument('--verbose', action='store_true',
                        help='show verbose output')
    parser.add_argument('-o', '--output-file', metavar='FILE',
                        help='write TREC run output to FILE')
    parser.add_argument('train_file')
    parser.add_argument('valid_file')
    parser.add_argument('test_file')
    parser.add_argument('test_file_result')
    parser.add_argument('test_file_directory')
    args = parser.parse_args()

    X, y, qid = load_svmlight_file(args.train_file, query_id=True)
    X_test, y_test, qid_test = load_svmlight_file(args.test_file, query_id=True)
    #print(qid_test)
    #print (type(X))
    #print (X)
    model = CoordinateAscent(n_restarts=1,
                             max_iter=25,
                             verbose=args.verbose,
                             scorer=NDCGScorer(k=10, idcg_cache={}))

    if args.no_validation or args.valid_file == '':
        model.fit(X, y, qid)
    else:
        X_valid, y_valid, qid_valid = load_svmlight_file(args.valid_file, query_id=True)
        model.fit(X, y, qid, X_valid, y_valid, qid_valid)


    pred = model.predict(X_test, qid_test)
    #print (pred)
    #print (model.coef_)


    #for k in (10, 20):
    result = ''
    result_ndcg_10_detail = ''
    result_ndcg_20_detail = ''

    score = NDCGScorer(k=10)(y_test, pred, qid_test).mean()
    result+=str(score) + '\t'

    scores = NDCGScorer(k=10)(y_test, pred, qid_test)
    for score in scores:
        result_ndcg_10_detail+=str(score) + "\t"
    #print (scores)

    score = NDCGScorer(k=20)(y_test, pred, qid_test).mean()
    result += str(score)

    scores = NDCGScorer(k=20)(y_test, pred, qid_test)
    for score in scores:
        result_ndcg_20_detail+=str(score) + "\t"

    result_ndcg_10_detail = result_ndcg_10_detail.strip()
    result_ndcg_20_detail = result_ndcg_20_detail.strip()

    print(result)
    print(result_ndcg_10_detail)
    print(result_ndcg_20_detail)

    number_of_fields = 13
    run_dict = load_run_into_dictionary(args.test_file_result, number_of_fields)

    fsdm_file = open(args.test_file_directory + "/fsdm.run", "w")
    entity_semantic_file = open(args.test_file_directory + "/entity_semantic.run", "w")
    term_semantic_file = open(args.test_file_directory + "/term_semantic.run", "w")
    semantic_file = open(args.test_file_directory + "/semantic.run", "w")

    for query in run_dict.keys():
        count_doc_id = 1
        for doc in run_dict[query].keys():
            field_scores = run_dict[query][doc]
            semantic_score = 0
            entity_semantic_score = 0
            term_semantic_score = 0
            for i in np.arange(number_of_fields):
                semantic_score+= (model.coef_[i] * field_scores[i])
            for i in np.arange(7):
                entity_semantic_score+= (model.coef_[i] * field_scores[i])
            for i in np.arange(6, number_of_fields):
                term_semantic_score+= (model.coef_[i] * field_scores[i])
            fsdm_score = field_scores[6]
            semantic_string = query + " " + "Q0" + " " + doc + " " + str(count_doc_id) + " " + str(semantic_score) + " SEMANTIC\n"
            entity_semantic_string = query + " " + "Q0" + " " + doc + " " + str(count_doc_id) + " " + str(entity_semantic_score) + " ENTITY_SEMANTIC\n"
            term_semantic_string = query + " " + "Q0" + " " + doc + " " + str(count_doc_id) + " " + str(term_semantic_score) + " TERM_SEMANTIC\n"
            fsdm_string = query + " " + "Q0" + " " + doc + " " + str(count_doc_id) + " " + str(fsdm_score) + " FSDM\n"
            count_doc_id+=1
            semantic_file.write(semantic_string)
            entity_semantic_file.write(entity_semantic_string)
            term_semantic_file.write(term_semantic_string)
            fsdm_file.write(fsdm_string)


    semantic_file.close()
    entity_semantic_file.close()
    term_semantic_file.close()
    fsdm_file.close()

            #if args.output_file:
    #    docno = load_docno(args.test_file, letor=True)
    #    print_trec_run(qid_test, docno, pred, output=open(args.output_file, 'wb'))
    # attributes names related_entities category text redirect fsdm
    #[0.04123711  0.0353461   0.05301915  0.02945508  0.40648012  0.02798233   0.40648012]

if __name__ == '__main__':
    main()

#ca_learner.py -h -nv --verbose -o /home/smsarwar/results_ca.run /home/smsarwar/PycharmProjects/eyre18/data/processed/eyre/INEX_LD/fold0/train_file /home/smsarwar/PycharmProjects/eyre18/data/processed/eyre/INEX_LD/fold0/validation_file /home/smsarwar/PycharmProjects/eyre18/data/processed/eyre/INEX_LD/fold0/test_file




