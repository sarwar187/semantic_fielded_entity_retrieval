import gensim
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
print gensim.models.word2vec.FAST_VERSION

def read_input(input_file):
    """This method reads the input file which is in gzip format""" 
    logging.info("reading file {0}...this may take a while".format(input_file))
    with open(input_file, 'r') as f:
        for i, line in enumerate(f):
 
            if (i % 10000 == 0):
                logging.info("read {0} reviews".format(i))
            # do some pre-processing and return list of words for each review
            # text
            yield line.split(' ')

data_file = '../../data/corpus/wikipedia_entity_corpus_sample.txt'
documents = list (read_input (data_file))
logging.info ("Done reading data file")
print len(documents)

model = gensim.models.Word2Vec (documents, size=200, window=10, min_count=0, workers=48,sg=1)
model.train(documents,total_examples=len(documents),epochs=10)
model.save('../../data/embedding/entity2vec_model_skip-gram_10epoch.bin')
