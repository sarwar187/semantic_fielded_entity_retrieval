import tqdm
import operator
import sys


"""
Not necessary
"""
def include_score_from_file(file_name, baseline, weight):
    print file_name
    with open(file_name) as f:
        for line in f:
            print line
            splitted = line.strip().split(' ')
            query = splitted[0]
            entity = splitted[2]
            score = float(splitted[4])
            if query in baseline.keys():
                if entity in baseline[query].keys():
                    baseline[query][entity]+= weight * score
                else:
                    baseline[query][entity] = 0
                    baseline[query][entity]+= weight * score
            else:
                baseline[query] = {}
                baseline[query][entity] = 0
                baseline[query][entity]+= weight * score
# Normalize the word matching retrieval results

def dump_result_to_file(baseline):
    output = open('../combined.txt','w')
    for q in tqdm.tqdm(baseline):
        sorted_x = sorted(baseline[q].items(), key=operator.itemgetter(1), reverse=True)
        for i in range(len(sorted_x)):
            output.write('%s Q0 %s %d %f combined_fsdm_semantic\n'%(q, sorted_x[i][0], i, sorted_x[i][1]))
    output.close()


def main(argv):
    prg_name = sys.argv[0]
    file1_name = sys.argv[1]
    file1_weight = float(sys.argv[2])
    file2_name = sys.argv[3]
    file2_weight = float(sys.argv[4])
    baseline = {}
    include_score_from_file(file1_name, baseline, file1_weight)
    include_score_from_file(file2_name, baseline, file2_weight)
    dump_result_to_file(baseline)

if __name__ == "__main__":
    main(sys.argv)