import tqdm
import operator
import sys
import os

def normalize_scores_and_write_to_file(file_name):
    baseline = {}

    with open(file_name) as f:
        for line in f:
            splitted = line.strip().split(' ')
            query = splitted[0]
            entity = splitted[2]
            score = float(splitted[4])
            baseline.setdefault(query, {})
            baseline[query][entity] = score

    # Normalize the word matching retrieval results
    normalized_result = {}
    for q in baseline:
        retrieval_scores = []
        for d in baseline[q]:
            retrieval_scores.append(baseline[q][d])
        min_score = min(retrieval_scores)
        max_score = max(retrieval_scores)
        normalized_result.setdefault(q,{})
        for d in baseline[q]:
            normalized_result[q][d] = float(baseline[q][d] - min_score) / float(max_score-min_score)

    #print os.path.splitext(file_name)[0] + '.jpg'
    output = open(os.path.splitext(file_name)[0] + '_normalized.run') # open('normalized_' + file_name + '.txt', 'w')
    run_name = "normalized_" + os.path.splitext(file_name)[0]
    #output = open('normalized_' + file_name + '.txt','w')
    for q in tqdm.tqdm(normalized_result):
        sorted_x = sorted(normalized_result[q].items(), key=operator.itemgetter(1), reverse=True)
        for i in range(len(sorted_x)):
            output.write('%s Q0 %s %d %f %s\n'%(q, sorted_x[i][0], i, sorted_x[i][1], run_name))
    output.close()


def main(argv):
    prg_name = sys.argv[0]
    file_name = sys.argv[1]
    normalize_scores_and_write_to_file(file_name)

if __name__=="__main__":
    main(sys.argv)