import os

field_names = ['category', 'redirect', 'names', 'related_entities', 'text', 'attribute', 'query', 'term_query']


for field_name in field_names:
    directory = "../../data/"
    str = ''
    str+= 'srun --mem=120G python2.7 embed_document.py '
    str+= directory + 'corpus/corpus.txt '
    str+= directory + 'field_embeddings/category/category_embedding.csv '
    str+= 'category '
    str+= '200 '
    str+= directory + 'embedding/phrase2vec_model_skip-gram_10epoch.bin '
    str+= directory + 'query_string_file '
    str+= directory + 'document_keys.csv'

    print str
