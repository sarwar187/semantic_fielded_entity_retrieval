import sys
sys.path.append('..')
sys.path.append('../..')
sys.path.append('../../..')
import json
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import numpy as np
from code.models.compute_embedding import compute_embedding
import csv
import os
import psutil

def process_doc(doc, target_document_set, original_dictionary):
    json_document = json.loads(doc)
    flag = 0
    if json_document.keys()[0] in target_document_set:
        flag = 1
        original_dictionary.update(json_document)
    return flag


def process_doc_field(doc, field_name, embedding_size, compute_embedding_cls, keys_file):
    """
    This function embeds a field of a document and returns the embedding vector
    :param doc: the dbpedia document we are creating embedding for
    :param field_name: the field (for example "category") for which we are computing embedding
    :param embedding_size: size of embedding (200 for this project)
    :param compute_embedding_cls: (a embedding class that generates average embedding)
    :param keys_file: location of the keys file where document keys will be stored (to get the document id and query ids)
    :return: embedding vector
    """
    json_document = json.loads(doc)
    key = json_document.keys()[0]
    keys_file.write(key + "\n")
    mismatch_count = 0
    match_count = 0
    missing_field_count = 0
    if field_name == "query":
        field_value = json_document[key]["query"]
        field_value = field_value.strip()
        # print field_value
        field_value_tokenized = field_value.split(" ")
        embedding, micount, macount = compute_embedding_cls.get_average_embedding(field_value_tokenized, embedding_size)
        mismatch_count += micount
        match_count += macount

    elif field_name in json_document[key].keys():
        field_value = ''
        if field_name=="text":
            field_value = json_document[key][field_name]
        else:
            for value in json_document[key][field_name]:
                field_value+= value + ' '
        field_value = field_value.strip()
        #print field_value
        field_value_tokenized = field_value.split(" ")
        #print len(field_value_tokenized)
        embedding, micount, macount  = compute_embedding_cls.get_average_embedding(field_value_tokenized, embedding_size)
        mismatch_count+=micount
        match_count+=macount
    else:
        embedding = np.zeros((1, embedding_size))
        #print embedding.shape
        embedding = embedding.flatten()
        #print embedding.shape
        missing_field_count += 1

    return embedding, mismatch_count, match_count, missing_field_count


def compute_number_of_documents(original_file):
    number_of_documents = 0
    for line in open(original_file):
        if line.startswith('    "<dbpedia:'):
            number_of_documents+=1
    print number_of_documents
    return number_of_documents


def compute_query_embedding(original_file, embedding_file, field_name, embedding_size, embedding_file_name, query_string_file, keys_file):
    json_document = json.load(open(original_file))
    compute_embedding_cls = compute_embedding(embedding_file_name)
    count = 0
    csvfile = open(embedding_file, 'w')
    writer = csv.writer(csvfile)
    mismatch_count = 0
    match_count = 0
    f = open(query_string_file, 'w')
    keys_file_pointer = open(keys_file, "w")
    for key in json_document:
        temp = dict()
        temp[key] = json_document[key]
        document = json.dumps(temp)
        embedding, micount, macount, missing_field_count = process_doc_field(document, field_name, embedding_size,
                                                            compute_embedding_cls, keys_file_pointer)
        mismatch_count += micount
        match_count += macount
        writer.writerow(embedding)
        print json_document[key]
        f.write(key + "\t" + json_document[key]["query"] + "\n")
        count += 1
        print "finished processing " + str(count) + " documents"
        print "mismatch count is " + str(mismatch_count)
        print "match count is " + str(match_count)
        mismatch_count = 0
        match_count = 0
    keys_file_pointer.close()
    csvfile.close()
    f.close()


def compute_field_embedding(original_file, embedding_file, field_name, embedding_size, embedding_file_name, keys_file):
    #file = "../../data/interim/corpus.txt"
    """
    :param original_file: collection file
    :param embedding_file: file to write field embedding
    :param field_name: name of the field to compute embedding for
    :param embedding_size: 200 by default
    :param embedding_file_name: name of the file where embeddeing of a field mentioned in by the field name parameter would be stored
    :param keys_file:
    """
    #matrix = np.zeros((compute_number_of_documents(original_file), embedding_size), dtype=float)
    process = psutil.Process(os.getpid())
    #print(process.memory_info().rss / 1000000000)
    compute_embedding_cls = compute_embedding(embedding_file_name)
    doc = ''
    count = 0
    started_reading = 0
    csvfile = open(embedding_file, 'w')
    writer = csv.writer(csvfile)
    mismatch_count = 0
    match_count = 0
    missing_field_count = 0
    keys_file_pointer = open(keys_file, "w")
    for line in open(original_file):
        if line.startswith('    "<dbpedia:'):
            started_reading = 1
        if line.startswith('    }'):
            #print  "stopped here 5"
            embedding, micount, macount, mfcount = process_doc_field("{" + doc + "}}", field_name, embedding_size, compute_embedding_cls, keys_file_pointer)
            #print  "stopped here 6"
            mismatch_count+=micount
            match_count+=macount
            missing_field_count+=mfcount
            #print embedding
            #print  "stopped here 3"
            writer.writerow(embedding)
            #print  "stopped here 4"
            #print "finished processing " + str(count) + " documents"
            count+=1
            doc = ''
            started_reading = 0
            if count % 1000 == 0 and count > 1000:
                print "finished processing " + str(count) + " documents"
                print("current memory allocation " + str(process.memory_info().rss / 1000000000))
                print "mismatch count is " + str(mismatch_count)
                print "match count is " + str(match_count)
                print "missing field count is " + str(missing_field_count)
                mismatch_count = 0
                match_count = 0


        if started_reading == 1:
            doc+=line

    #df = pd.DataFrame(matrix)
    #df.to_csv(embedding_file, header=None)
    #with open(embedding_file, 'w') as csvfile:
    #    writer = csv.writer(csvfile)
    #    [writer.writerow(matrix[row]) for row in np.arange(matrix.shape[0])]
    keys_file_pointer.close()
    csvfile.close()


def main(argv):
    prog_name = sys.argv[0]
    arg_cnt = len(sys.argv)
    #print(sys.argv)
    if arg_cnt != 8:
        print("Usage: python2.7 <original_file> <field_embedding_file> <field_name> <embedding_size> <word_embedding_file> <query_string_file> <keys_file>")
        sys.exit(1)

    original_file = sys.argv[1]
    field_embedding_file = sys.argv[2]
    field_name = sys.argv[3]
    embedding_size = int(sys.argv[4])
    word_embedding_file = sys.argv[5]
    query_string_file = sys.argv[6]
    keys_file = sys.argv[7]


    if field_name=='query':
        compute_query_embedding(original_file, field_embedding_file, field_name, embedding_size, word_embedding_file,
                                query_string_file, keys_file)
    else:
        compute_field_embedding(original_file, field_embedding_file, field_name, embedding_size, word_embedding_file, keys_file)

    #document_keys_file = "/home/smsarwar/query_keys.csv"
    #query_string_file = "/home/smsarwar/query_string_file"


    #dump_relevant_documents(original_file, filtered_file, qrel_file)
    #load_filtered_document(filtered_file)

if __name__ == "__main__":
    main(sys.argv)