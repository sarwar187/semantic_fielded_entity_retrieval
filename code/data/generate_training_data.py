import sys
import numpy as np
import json
import random
import sys
reload(sys)
sys.setdefaultencoding('utf8')
from tables.utils import string_to_classes

"""
This script is for generate the train, validation and test data cases for
"""

import math

def sigmoid(x):
  return 1 / (1 + math.exp(-x))


def load_qrel_into_dictionary(qrel_file):
    """
    This method loads a qrel file and returns a dictionary [query][document] = relevance
    :param qrel_file: file to load qrel from
    return qrel_dict
    """
    qrel_dict = {}
    with open(qrel_file) as f:
        for line in f:
            #print line
            splitted = line.strip().split('\t')
            query = splitted[0]
            entity = splitted[2]
            score = float(splitted[3])
            qrel_dict.setdefault(query, {})
            qrel_dict[query][entity] = score
    return qrel_dict


def load_aggregated_run_into_dictionary(run_file, number_of_fields):
    """
    This method loads a run file and returns a dictionary [query][document] = (score_tuple)
    :param run_file:
    """
    run_dict = {}
    with open(run_file) as f:
        for line in f:
            splitted = line.strip().split(' ')
            query = splitted[0]
            entity = splitted[2]
            field_scores = []
            #sum = 0
            for i in np.arange(number_of_fields):
                #sum+= sigmoid(float(splitted[i+4]))
                field_scores.append(float(splitted[i+4]))
            run_dict.setdefault(query, {})
            run_dict[query][entity] = field_scores
    return run_dict


def load_run_into_dictionary(run_file, number_of_fields):
    """
    This method loads a run file and returns a dictionary [query][document] = (score_tuple)
    :param run_file:
    """
    run_dict = {}
    with open(run_file) as f:
        for line in f:
            splitted = line.strip().split(' ')
            query = splitted[0]
            entity = splitted[2]
            field_scores = []
            for i in np.arange(number_of_fields):
                field_scores.append(float(splitted[i+4]))
            run_dict.setdefault(query, {})
            run_dict[query][entity] = field_scores
    return run_dict

def merge_dictionaries(run_dict1, run_dict2):
    for query in run_dict1.keys():
        for doc in run_dict1[query].keys():
            run_dict1[query][doc] = run_dict1[query][doc] + run_dict2[query][doc]
    return run_dict1

def refine_run(qrel_dict, run_dict):
    """
    This method will refine the contents of run_dict and create a dictionary [query][document] = (relevance, score) tuple
    :param qrel_dict:
    :param run_dict:
    """
    run_dict_refined = {}
    run_dict_non_refined = {}
    for query in run_dict.keys():
        docs_qrel = set(qrel_dict[query].keys())
        docs_run = set(run_dict[query].keys())

        docs_common = docs_qrel.intersection(docs_run)
        # for doc in docs_common:
        #     run_dict_refined.setdefault(query, {})
        #     run_dict_refined[query][doc] = (run_dict[query][doc], qrel_dict[query][doc])
        #
        for doc in run_dict[query].keys():
            if doc in docs_common:
                run_dict_refined.setdefault(query, {})
                run_dict_non_refined.setdefault(query, {})
                run_dict_refined[query][doc] = (run_dict[query][doc], qrel_dict[query][doc])
                run_dict_non_refined[query][doc] = (run_dict[query][doc], qrel_dict[query][doc])
            else:
                #print query
                run_dict_non_refined.setdefault(query, {})
                run_dict_non_refined[query][doc] = (run_dict[query][doc], 0)
    return run_dict_refined, run_dict_non_refined


def load_json_into_dictionary(json_file, query_type, run_dict_refined, run_dict_non_refined, number_of_fields):
    """
    This function will load a json file into dictionary [fold][data_type] = [query_ids] -> ["0"]["testing"], ["0"]["training"]
    :param json_file:
    """
    #for each fold

    #create training data
    #create validation data
    #create test data
    #create them using svmlight format -1 1:0.43 3:0.12 9284:0.2 # abcdef
    index_to_remove=[0,1,]
    with open(json_file) as f:
        data = json.load(f)

    for fold in data.keys():
        training_qid = data[fold]["training"]
        validation_qid = training_qid[:10]
        training_qid = training_qid[10:]
        testing_qid = data[fold]["testing"]
        training_file_location = "../../data/processed/eyre/" + query_type + "/fold" + fold + "/train_file"
        validation_file_location = "../../data/processed/eyre/" + query_type + "/fold" + fold + "/validation_file"
        test_file_location = "../../data/processed/eyre/" + query_type + "/fold" + fold + "/test_file"
        test_file_location_non_svmlight = "../../data/processed/eyre/" + query_type + "/fold" + fold + "/test_non_svmlight_file"
        training_file = open(training_file_location, "w")
        validation_file = open(validation_file_location, "w")
        test_file = open(test_file_location, "w")
        test_file_non_svmlight = open(test_file_location_non_svmlight, "w")

        count=1
        for qid in training_qid:
            for doc in run_dict_refined[qid]:
                tuple = run_dict_refined[qid][doc]
                string_to_write = str(int(float(tuple[1]))) + " qid:" + str(count) + " "
                for i in np.arange(number_of_fields):
                    string_to_write += str(i) + ":" + str(tuple[0][i]) + " "
                string_to_write+= "#abc\n"
                training_file.write(string_to_write)
            count+=1
        training_file.close()

        count=1
        for qid in validation_qid:
            for doc in run_dict_refined[qid]:
                tuple = run_dict_refined[qid][doc]
                string_to_write = str(int(float(tuple[1]))) + " qid:" + str(count) + " "
                for i in np.arange(number_of_fields):
                    string_to_write+= str(i) + ":" + str(tuple[0][i]) + " "
                string_to_write+= "#abc\n"
                validation_file.write(string_to_write)
            count+=1
        validation_file.close()

        # count=1
        # for qid in testing_qid:
        #     for doc in run_dict_refined[qid]:
        #         tuple = run_dict_refined[qid][doc]
        #         string_to_write = str(int(float(tuple[1]))) + " qid:" + str(count) + " "
        #         for i in np.arange(number_of_fields):
        #             string_to_write += str(i) + ":" + str(tuple[0][i]) + " "
        #         string_to_write+= "#abc\n"
        #         test_file.write(string_to_write)
        #     count+=1
        # test_file.close()

        count=1
        for qid in testing_qid:
            count_doc_id = 1
            for doc in run_dict_non_refined[qid]:
                tuple = run_dict_non_refined[qid][doc]
                string_to_write = str(int(float(tuple[1]))) + " qid:" + str(count) + " "
                string_to_write_non_svmlight = qid + " " + "Q0" + " " + doc + " " + str(count_doc_id) + " "
                for i in np.arange(number_of_fields):
                    string_to_write += str(i) + ":" + str(tuple[0][i]) + " "
                    string_to_write_non_svmlight += str(tuple[0][i]) + " "
                string_to_write+= "#abc\n"
                string_to_write_non_svmlight += "FSDM\n"
                test_file.write(string_to_write)
                test_file_non_svmlight.write(string_to_write_non_svmlight)
                count_doc_id+=1
            count+=1
        test_file.close()

def main(argv):
    query_types = ["INEX_LD", "ListSearch", "QALD2", "SemSearch-ES", "all_queries"]
    #python2.7 generate_training_data.py
    # program_name = sys.argv[0]
    # qrel_file = sys.argv[1]
    # run_file = sys.argv[2]
    # json_file = sys.argv[3]
    # number_of_fields = sys.argv[4]
    #attributes, names, related_entities, category, text, redirect, fsdm
    qrel_file = "../../data/processed/eyre/qrels-v2.txt"
    #qrel_file = "../../data/processed/eyre/qrel_no_redirect.txt"
    semantic_entity_run_file = "../../data/processed/eyre/semantic_component_score_v2.run"
    fsdm_run_file = "../../data/processed/eyre/normalized_fsdm_v2.run.txt"
    #fsdm_run_file = "../../data/processed/eyre/fsdm_v2.run"
    semantic_term_run_file = "../../data/processed/eyre/term_semantic_component_score_v2.run"
    #run_file = "../../data/processed/eyre/combined.run"

    for query_type in query_types:
        json_file = "../../data/processed/eyre/folds/" + query_type +".json"
        qrel_dict = load_qrel_into_dictionary(qrel_file)
        print 'qrel loaded'
        number_of_fields = 6
        #semantic_entity_run_dict = load_aggregated_run_into_dictionary(semantic_entity_run_file, number_of_fields)
        semantic_entity_run_dict = load_run_into_dictionary(semantic_entity_run_file, number_of_fields)
        print 'semantic entity run loaded'
        #semantic_term_run_dict = load_aggregated_run_into_dictionary(semantic_term_run_file, number_of_fields)
        semantic_term_run_dict = load_run_into_dictionary(semantic_term_run_file, number_of_fields)
        print 'semantic term run loaded'
        number_of_fields = 1
        #fsdm_run_dict = load_aggregated_run_into_dictionary(fsdm_run_file, number_of_fields)
        fsdm_run_dict = load_run_into_dictionary(fsdm_run_file, number_of_fields)
        print 'fsdm run loaded'
        run_dict = merge_dictionaries(semantic_entity_run_dict, fsdm_run_dict)
        run_dict = merge_dictionaries(run_dict, semantic_term_run_dict)
        print 'dictionaries merged'
        run_dict_refined, run_dict_non_refined = refine_run(qrel_dict, run_dict)
        print 'run refined'
        number_of_fields = 13
        load_json_into_dictionary(json_file, query_type, run_dict_refined, run_dict_non_refined, number_of_fields)
        print 'all done'

if __name__=="__main__":
    main(sys.argv)