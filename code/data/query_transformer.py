import tagme
import re
import string
import json
# Set the authorization token for subsequent calls.
tagme.GCUBE_TOKEN = "58f5b784-bf5a-4840-a928-e214134f98e7-843339462"
regex = re.compile('[%s]' % re.escape(string.punctuation))
query_entities = {}
query_file = "../../data/queries/queries-v2_stopped.txt"
query_file_dump = "../../data/queries/queries.json"
term_query_file_dump = "../../queries/term_queries.json"

query_term_dictionary = {}
query_dictionary = {}
query_dictionary_dump = {}
for line in open(query_file):
    line_splitted = line.split("\t")
    query_dictionary[line_splitted[0]] = line_splitted[1]
    query_term_dictionary[line_splitted[0]]={}
    query_term_dictionary[line_splitted[0]]["query"] = line_splitted[1]


#this file would contain queries with only terms
with open(term_query_file_dump, 'w') as fp:
    json.dump(query_term_dictionary, fp)

#this file would contain queries with only entities
for key in query_dictionary:
    #print q, original_queries[q]
    print key
    q = query_dictionary[key]
    annotations = tagme.annotate(query_dictionary[key])
    query_string = ''
    for ann in annotations.get_annotations(0.1):
        #print ann.entity_title
        query_entities.setdefault(q, {})
        entity = ann.entity_title.lower()
        entity = entity.replace(' ', '_')
        query_entities[q][entity] = 1
        query_string+= entity + ' '
        print entity
    query_string = query_string.strip()
    print query_string
    #query_dictionary_dump[key] = query_string
    query_dictionary_dump[key] = dict()
    query_dictionary_dump[key]["query"] = query_string
    print '-------------'

with open(query_file_dump, 'w') as fp:
    json.dump(query_dictionary_dump, fp)


